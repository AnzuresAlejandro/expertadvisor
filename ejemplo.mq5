//+------------------------------------------------------------------+
//|                                                      ejemplo.mq5 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\SymbolInfo.mqh>
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   CSymbolInfo info_simbolo;
   info_simbolo.Name(_Symbol);
   info_simbolo.RefreshRates();
   
   Print("Simbolo:",info_simbolo.Name(),"\tDescripcion:\t",info_simbolo.Description());
   
   Print("Simbolo:",info_simbolo.Name(),"\tSPREAD:\t",info_simbolo.Digits());
   // Forma para parcear el spread de cada uno de las graficas
   Print("Simbolo:",info_simbolo.Name(),"\tSPREAD:\t",NormalizeDouble(info_simbolo.Spread()*info_simbolo.Point(),info_simbolo.Digits()));
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                             asesorcurso_v2.0.mq5 |
//|                                                      Osc.Rod.Fer |
//|                               http://www.bibliotecadetrading.com |
//+------------------------------------------------------------------+
#property copyright "Osc.Rod.Fer"
#property link      "http://www.bibliotecadetrading.com"
#property version   "2.00"

#include <Trade\Trade.mqh>
#include <Trade\PositionInfo.mqh>
#include <Trade\AccountInfo.mqh>
#include <Trade\SymbolInfo.mqh>

CTrade miTrade;
CPositionInfo misPosiciones;
CAccountInfo miCuenta;
CSymbolInfo miSimbolo;

int Min_Bars = 20;

input int NumMagico = 1234;
input double Lot = 0.1;
input ulong dev= 100;
input double StopLoss = 100;
input double TakeProfit = 240;
input int MA_Period = 15;
input int ADX_Period = 15;
input double Adx_Min = 24.0;

int adxHandle;
double plsDI[], minDI[], adxVal[];

int maHandle;
double maVal[];
double p_cierre;

//Funcion para chequea si esta permitido el tradeo en nuestra cuenta
bool chequeoTrading()
{
   bool siTrading = false;
   
   if(miCuenta.TradeAllowed() && miCuenta.TradeExpert() )
      {
      
      int mbars = Bars(Symbol(), Period() );
      if( mbars > Min_Bars)
         {
         siTrading = true;
         }
      }
      //Print(siTrading);
   return (siTrading);
}

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
      miSimbolo.Name(_Symbol);
      miTrade.SetExpertMagicNumber(NumMagico);
      miTrade.SetDeviationInPoints(dev);
      
      adxHandle=iADX(_Symbol, _Period, ADX_Period);
      maHandle=iMA(_Symbol, _Period, MA_Period, 0, MODE_EMA, PRICE_CLOSE);
      if(adxHandle < 0 || maHandle < 0)
         {
         Alert("Error al obtener los indicadores ADX y MA. Error: ", GetLastError(), "!!");
         return(1);
         }
  
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   IndicatorRelease(adxHandle);
   IndicatorRelease(maHandle);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   //bool trad = chequeoTrading();
   //Print("Trad ", trad);
   if(chequeoTrading() == false)
      {
      Print("Por algun motivo el trading no esta permitido", GetLastError() );
      return;
      }
      
      
   MqlRates mrate[];
   
   ArraySetAsSeries(mrate, true);
   ArraySetAsSeries(adxVal, true);
   ArraySetAsSeries(plsDI, true);
   ArraySetAsSeries(minDI, true);
   ArraySetAsSeries(maVal, true);
   
   if(!miSimbolo.RefreshRates())
      {
      Alert("Error cogiendo las ultimas quotas de precio - error", GetLastError());
      return;
      }
      
   if(CopyRates(_Symbol, _Period, 0, 3, mrate)<0)
      {
      Alert("Error copiando rates/history data - error:", GetLastError() );
      return;
      }
   
   //Detectamos una nueva barra
   static datetime Prev_time;
   datetime  Bar_time[1];
   Bar_time[0] = mrate[0].time;
   
   if(Prev_time==Bar_time[0])
      {
      return;
      }
   Prev_time = Bar_time[0];
   
   if((CopyBuffer(adxHandle, 0, 0, 3, adxVal) < 3) || (CopyBuffer(adxHandle, 1, 0, 3, plsDI) < 3) ||
      (CopyBuffer(adxHandle, 2, 0, 3, minDI) < 3) )
      {
      Alert("Error copiando el indicador buffer del ADX - error", GetLastError());
      return;
      }
   
   
   if(CopyBuffer(maHandle, 0, 0, 3, maVal) < 3)
      {
      Alert("Error copiando el indicador buffer de la Media - error", GetLastError());
      return;
      }
      
   p_cierre = mrate[1].close;
   
   bool Compra_abierta = false;
   bool Venta_abierta = false;
   
   if(misPosiciones.Select(_Symbol) == true )
      {
      if(misPosiciones.PositionType() == POSITION_TYPE_BUY)
         {
         Compra_abierta = true;
         if(CerrarPosicion("COMPRA", p_cierre) == true)
            {
            Compra_abierta =false;
            return;
            }
         }
         else if(misPosiciones.PositionType() == POSITION_TYPE_SELL)
          {
          Venta_abierta = true;
          //Alert("HAY UNA POSICION DE VENTA");
          if(CerrarPosicion("VENTA", p_cierre) == true)
            {
            Venta_abierta = false;
            return;
            }
          }
      }
      
      
//Comprobamos si podemos comprar
   if(compruebaCompra() == true)
      {
      if(Compra_abierta)
         {
         //Alert("Ya tenemos una posicion de compra abierta");
         return;
         }
         
      double mprice = NormalizeDouble(miSimbolo.Ask(), _Digits);
      double stloss = NormalizeDouble(miSimbolo.Ask() - StopLoss * _Point, _Digits);
      double tprofit = NormalizeDouble(miSimbolo.Ask() + TakeProfit * _Point, _Digits);
      
      if(miTrade.Buy(Lot, _Symbol, mprice, stloss, tprofit))
         {
         Alert("La orden de compra se ha realizado correctamente con el ticket#: ",
               miTrade.ResultDeal(), "!!");
         
         }
         else {
            Alert("La orden de compra con el volumen: ", miTrade.RequestVolume(),
            ", sl:", miTrade.RequestSL(), ", tp: ", miTrade.RequestTP(),
            ", precio: ", miTrade.RequestPrice(), 
            " no se ha podido realizar por el error - ", miTrade.ResultRetcodeDescription());
            return;
         }
      }
   
//Comprobamos si podemos vender   
   if(compruebaVenta() == true)
      {
      if(Venta_abierta)
         {
         //Alert("Ya tenemos una posicion de compra abierta");
         return;
         }
         
      double sprice = NormalizeDouble(miSimbolo.Bid(), _Digits);
      double sstloss = NormalizeDouble(miSimbolo.Bid() + StopLoss * _Point, _Digits);
      double stprofit = NormalizeDouble(miSimbolo.Bid() - TakeProfit * _Point, _Digits);
      
      if(miTrade.Sell(Lot, _Symbol, sprice, sstloss, stprofit))
         {
         Alert("La orden de venta se ha realizado correctamente con el ticket#: ",
               miTrade.ResultDeal(), "!!");
         
         }
         else {
            Alert("La orden de venta con el volumen: ", miTrade.RequestVolume(),
            ", sl:", miTrade.RequestSL(), ", tp: ", miTrade.RequestTP(),
            ", precio: ", miTrade.RequestPrice(), 
            " no se ha podido realizar por el error - ", miTrade.ResultRetcodeDescription());
            return;
         }
      }
  }
//+------------------------------------------------------------------+

bool compruebaCompra(){

   bool comprar = false;
   //Comment("Entrar al mercado en Compra +DI:", plsDI[1], " -DI: ", minDI[1] );
   if((maVal[0]>maVal[1]) && (maVal[1]>maVal[2]) && (p_cierre > maVal[1]))
      {
      if( (adxVal[1] > Adx_Min) && (plsDI[1]>minDI[1]) ){
        //Alert("Entrar al mercado en Compra");
        Comment("Entrar al mercado en Compra");
        comprar = true;
        }
      }
      
   return (comprar);
}

bool compruebaVenta(){

   bool vender = false;
   //Comment("Entrar al mercado en Venta +DI:", plsDI[1], " -DI: ", minDI[1] );
   if((maVal[0]<maVal[1]) && (maVal[1]<maVal[2]) && (p_cierre < maVal[1]))
      {
      if( (adxVal[1] > Adx_Min) && (minDI[1] > plsDI[1]) ){
        //Alert("Entrar al mercado en Venta");
        Comment("Entrar al mercado en Venta");
        vender = true;
        }
      }
      
   return (vender);
}

bool CerrarPosicion( string ptipo, double preciocierre){
   bool cerrada = false;
   
   if(misPosiciones.Select(Symbol()) == true)
   {
      if( misPosiciones.Magic() == NumMagico && misPosiciones.Symbol() == Symbol())
      {
         //Condiciones de cierre para las compras
         if( ptipo == "COMPRA")
            {
            if(preciocierre < maVal[1])
               {
               if(miTrade.PositionClose(Symbol()) == true)
                  {
                  Alert("La posicion ha sido cerrada correctamente.");
                  cerrada = true;
                  }
               }
            
            }
         //Condiciones de cierre para las ventas
         if( ptipo == "VENTA")
            {
            if(preciocierre > maVal[1])
               {
               if(miTrade.PositionClose(Symbol()) == true)
                  {
                  Alert("La posicion ha sido cerrada correctamente.");
                  cerrada = true;
                  }
               }
            }
      }
   }
   return (cerrada);
}
//+------------------------------------------------------------------+
//|                                             asesorcurso_v0.0.mq5 |
//|                                                      Osc.Rod.Fer |
//|                               http://www.bibliotecadetrading.com |
//+------------------------------------------------------------------+
#property copyright "Osc.Rod.Fer"
#property link      "http://www.bibliotecadetrading.com"
#property version   "1.00"

#include <Trade\Trade.mqh>

#include <Trade\PositionInfo.mqh>
#include <Trade\AccountInfo.mqh>
#include <Trade\SymbolInfo.mqh>

CTrade miTrade;
CPositionInfo misPosiciones;
CAccountInfo miCuenta;
CSymbolInfo miSimbolo;

int Min_Bars = 20;

input int NumMagico = 1234;
input double Lot = 0.1;
input ulong dev= 100;
input double StopLoss = 100;
input double TakeProfit = 240;
input int MA_Period = 15;

int maHandle;
double maVal[];
double p_cierre;



//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   miSimbolo.Name(_Symbol); // Agregamos el nombre del simbolo al nombre Simbolo al nombre
   miTrade.SetExpertMagicNumber(NumMagico); // Agregamos un identificador
   miTrade.SetDeviationInPoints(dev);  // establece la desviacion permitida para poder operar
   
   maHandle=iMA(_Symbol, Period(), MA_Period, 0, MODE_EMA, PRICE_CLOSE); // Declarar el manejador de la MA
   if(maHandle < 0)
      {
      return(INIT_FAILED);
      }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   IndicatorRelease(maHandle);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   //bool trad = chequeoTrading();
   //Print("Trad ", trad);
   // Revisa si se puede hace trading 
   if(chequeoTrading() == false)
      {
      Print("Por algun motivo el trading no esta permitido", GetLastError() );
      return;
      }
   // Declaramos la    
   MqlRates mrate[];
   // Invierte el arreglo con el fin de no tener que preguntar en todo momento cual por el ultimo elemento 
   ArraySetAsSeries(mrate, true);
   ArraySetAsSeries(maVal, true);
   
   // Actualiza la informacion del simbolo
   if(!miSimbolo.RefreshRates())
      {
      Alert("Error cogiendo la ultima quota de precios - error:", GetLastError() );
      return;
      }
   if(CopyRates(_Symbol, _Period, 0, 3, mrate)<0)
      {
      Alert("Error copiando rates/history data - error:", GetLastError() );
      return;
      }
   //------------------------------------------------------------------------
   //------todo este bloque sirve para poder identificar nuevas barras-------
   //------------------------------------------------------------------------
   static datetime Prev_time;
   datetime  Bar_time[1];
   Bar_time[0] = mrate[0].time;
   if(Prev_time==Bar_time[0])
      {
      return;
      }
   Prev_time = Bar_time[0];
   
   //-------ESTO SIRVE PARA PODER  COPIAR EN UN ARRAY ELEMENTOS DE LA MEDIA MOVIL----------------
   
   //+------------------------------------------------------------------+
   //| CopyBuffer                                                       |
   //| int  CopyBuffer( 
   //| int       indicator_handle,     // manejador del indicador                                                                   |
   //| int       buffer_num,           // número del buffer del indicador
   //| int       numero de valores                           
   //| double arreglo al que se copiara la informacion
   //+------------------------------------------------------------------+
   
   if(CopyBuffer(maHandle, 0, 0, 3, maVal) < 3)
      {
      Alert("Error copiando el indicador buffer de la Media - error", GetLastError());
      return;
      }
   // Copia el valor del precio de cierre de la penultima vela en la variable p_cierre
      
   p_cierre = mrate[1].close;
   
   if((maVal[0]>maVal[1]) && (maVal[1]>maVal[2]) && (p_cierre > maVal[1]))
      {
      Alert("Entrar al mercado en Compra");
      Comment("Entrar al mercado en Compra");
      }
      else
         {
         Comment("Permanecer quieto");
         }
         //*/
         
  }
//+------------------------------------------------------------------+

//Funcion para chequea si esta permitido el tradeo en nuestra cuenta
bool chequeoTrading()
{
   bool siTrading = false;
   // Pregunta si permite el trader y ademas si la cuenta permite el trader con assesores expertos 
   if(miCuenta.TradeAllowed() && miCuenta.TradeExpert() )
      {
      // revisa si el numero de velas pueden ser operadas
      int mbars = Bars(Symbol(), Period() );
      if( mbars > Min_Bars)
         {
         siTrading = true;
         }
      }
      //Print(siTrading);
   return (siTrading);
}
//+------------------------------------------------------------------+
//|                                                    Primer_EA.mq5 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
//--- input parameters
input int      StopLoss=30;
input int      TakeProfit=100;
input int      ADX_Period=8;
input int      MA_Period=8;
input int      EA_Magic=12345;   // Magic Number EA
input double   Adx_Min=22.0;     // El valor mínimo de ADX
input double   Lot=0.1;          // Lotes de trading
//--- Otros parámetros
int adxHandle; // identificador para nuestro indicador ADX
int maHandle;  // identificador para nuestro indicador Moving Average
double plsDI[],minDI[],adxVal[]; // Arrays dinámicos para guardar los valores de +DI, -DI y los valores de ADX para cada barra
double maVal[]; // Array dinámico para guardar los valores de Moving Average para cada barra
double p_close; // Variable para almacenar el valor de la barra de cierre
int STP, TKP;   // A utilizar con los valores de Stop Loss y Take Profit
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   //--- Obtener el identificador para el indicador ADX
   adxHandle=iADX(NULL,0,ADX_Period);
//--- Obtener el identificador para el indicador Moving Average iMA es la funcion para poder obtener el identificadpr de 
//--- una media movil y los distintos parametros es para saber el simbolo, el periodo, el desplazamiento, el modo de la MA
//--- y el precio de la ma 
   maHandle=iMA(_Symbol,_Period,MA_Period,0,MODE_EMA,PRICE_CLOSE);
//--- Qué pasa si el identificador devuelve un valor no valido
   if(adxHandle<0 || maHandle<0)
     {
      Alert("Ha ocurrido un error al crear los identificadores de los indicadores -error: ",GetLastError(),"!!");
     }
     
     //--- Vamos a procesar pares de divisas con precios de 5 o 3 dígitos en lugar de 4
   STP = StopLoss;
   TKP = TakeProfit;
   if(_Digits==5 || _Digits==3)
     {
      STP = STP*10;
      TKP = TKP*10;
     }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
      IndicatorRelease(adxHandle);
      IndicatorRelease(maHandle);
      //--- Obtener la última cotización de precio mediante la estructura MQL5 MqlTick
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---

// ¿Tenemos bastantes barras para trabajar con ellas?
   if(Bars(_Symbol,_Period)<60) // si el número de barras es inferior a 60 barras 
     {
      Alert("Tenemos menos de 60 barras, EA va a salir ahora!!");
      return;
     }
// Usaremos la variable estática Old_Time para el tiempo de barra.
// A cada ejecución de OnTick compararemos el tiempo de barra actual con el que está guardado.
// Si el tiempo de barra no es igual al tiempo guardado, significa que tenemos un nuevo tick.
   static datetime Old_Time;
   datetime New_Time[1];
   bool IsNewBar=false;

// copiando el último tiempo de barra al elemento New_Time[0]
   int copied=CopyTime(_Symbol,_Period,0,1,New_Time);
   if(copied>0) // de acuerdo, se han copiado los datos con éxito
     {
      if(Old_Time!=New_Time[0]) // si el tiempo anterior no es igual al nuevo tiempo de barra
        {
         IsNewBar=true;   // si no es la primera llamada, la nueva barra ha aparecido
         if(MQL5InfoInteger(MQL5_DEBUGGING)) Print("Tenemos una nueva barra aquí",New_Time[0]," el tiempo anterior era ",Old_Time);
         Old_Time=New_Time[0];            // guardando el tiempo de barra
        }
     }
   else
     {
      Alert("Ha ocurrido un error al copiar los datos del historial de los tiempos, error =",GetLastError());
      ResetLastError();
      return;
     }

//--- EA debe comprobar si hay una nueva operación de trading, sólo si hay una barra nueva
   if(IsNewBar==false)
     {
      return;
     }
//--- Define algunas estructuras MQL5 que usaremos en nuestro trading
   MqlTick latest_price;     // Se utiliza para obtener las recientes/últimas cotizaciones de precios
   MqlTradeRequest mrequest;  // Se utiliza para enviar nuestras peticiones de trading
   MqlTradeResult mresult;    // Se utiliza para obtener los resultados de nuestro trading
   MqlRates mrate[];         // Se utiliza para almacenar los precios, los volúmenes y el diferencial de cada barra
   ZeroMemory(mrequest);  // Inicialización de la estructura mrequest
   /*
     Vamos a asegurarnos de que los valores de nuestros arrays para las Tasas "Rates", valores de ADX y valores de MA se almacenan con la misma indexación que los arrays de las series de tiempo
*/
// los arrays de tasas
   ArraySetAsSeries(mrate,true);
// el array de los valores de ADX DI+
   ArraySetAsSeries(plsDI,true);
// el array de los valores de ADX DI-
   ArraySetAsSeries(minDI,true);
// los arrays de los valores de ADX
   ArraySetAsSeries(adxVal,true);
// los arrays de los valores de MA-8
   ArraySetAsSeries(maVal,true);
   if(!SymbolInfoTick(_Symbol,latest_price))
     {
      Alert("Ha ocurrido un error al obtener la última cotización de precio -error:",GetLastError(),"!!");
      return;
     }
   if(CopyRates(_Symbol,_Period,0,3,mrate)<0)
  {
      Alert("Ha ocurrido un error al copiar los datos tasas/historial -error:",GetLastError(),"!!");
      return;
     }
     
     //--- Copiar los nuevos valores de nuestros indicadores a los buffers (arrays) mediante el identificador
   if(CopyBuffer(adxHandle,0,0,3,adxVal)<0 || CopyBuffer(adxHandle,1,0,3,plsDI)<0
      || CopyBuffer(adxHandle,2,0,3,minDI)<0)
     {
      Alert("Ha ocurrido un error al copiar los buffers del indicador ADX -error:",GetLastError(),"!!");
      return;
     }
   if(CopyBuffer(maHandle,0,0,3,maVal)<0)
     {
      Alert("Ha ocurrido un error al copiar el buffer del indicador Moving Average indicator -error:",GetLastError());
      return;
     }

  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---
   
  }
//+------------------------------------------------------------------+
//| TradeTransaction function                                        |
//+------------------------------------------------------------------+
void OnTradeTransaction(const MqlTradeTransaction& trans,
                        const MqlTradeRequest& request,
                        const MqlTradeResult& result)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
//---
   
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                      Beatriz.mq5 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_color1 clrRed,clrBlue,clrGreen,clrYellow,clrMagenta,clrCyan

/**
* Importar clases escenciales
*/
#include <Trade\Trade.mqh>
#include <Trade\PositionInfo.mqh>
#include <Trade\AccountInfo.mqh>
#include <Trade\SymbolInfo.mqh>

/**
* Definicion de clases principales
*/
CTrade miTrade;
CPositionInfo misPosiciones;
CAccountInfo miCuenta;
CSymbolInfo miSimbolo;

int Min_Bars=20;

input int NumMagico=1234;
input double Lot=0.1;
input ulong dev=100;
input double StopLoss=2;
input double TakeProfit=400;

//input int MA_PeriodR=12;
//
//input int MA_PeriodM=24;
//
//input int MA_PeriodL=50;
input int MA_PeriodR=8;

input int MA_PeriodM=16;

input int MA_PeriodL=24;

int operationClose ;

int maHandleR;
int maHandleM;
int maHandleL;
double maValR[];
double maValM[];
double maValL[];
double p_cierre;
bool ventaActiva;
bool compraActiva;
bool cruceAlcista;
bool cruceBajista;
bool posisionAbierta;
bool lateralidad;
double StopLossCalculado;
ulong ticket;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   miSimbolo.Name(_Symbol); // Agregamos el nombre del simbolo al nombre Simbolo al nombre
   miTrade.SetExpertMagicNumber(NumMagico); // Agregamos un identificador
   miTrade.SetDeviationInPoints(dev);  // establece la desviacion permitida para poder operar

   maHandleL=iMA(_Symbol, Period(), MA_PeriodL, 0, MODE_SMA, PRICE_CLOSE); // Declarar el manejador de la MA
   maHandleM=iMA(_Symbol, Period(), MA_PeriodM, 0, MODE_SMA, PRICE_CLOSE); // Declarar el manejador de la MA
   maHandleR=iMA(_Symbol, Period(), MA_PeriodR, 0, MODE_SMA, PRICE_CLOSE); // Declarar el manejador de la MA
                                                                           //PlotIndexSetInteger(0,PLOT_LINE_COLOR,0,clrAqua); 
   if(maHandleL<0 && maHandleM<0 && maHandleR<0)
     {
      return(INIT_FAILED);
     }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool revisar()
  {
   bool siTrading=false;
   if(miCuenta.TradeAllowed() && miCuenta.TradeExpert() && miSimbolo.IsSynchronized())
     {
      int mbars=Bars(_Symbol,_Period);
      if(mbars>Min_Bars)
        {
         siTrading=true;
        }
     }
   return siTrading;
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

   IndicatorRelease(maHandleL);
   IndicatorRelease(maHandleM);
   IndicatorRelease(maHandleR);

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   if(revisar()==false)
     {
      Print("Por algun motivo el trading no esta permitido",GetLastError());
      return;
     }
     //Comment("Stop Loss minimo = ",SymbolInfoInteger(_Symbol,SYMBOL_TRADE_STOPS_LEVEL));  // En teoria esto sirve para poder saber cual es la restriccion del Stoploss 
     // lo cual te ayudara a seguir el precio 
// Declaramos la    
   MqlRates mrate[];
// Invierte el arreglo con el fin de no tener que preguntar en todo momento cual por el ultimo elemento 
   ArraySetAsSeries(mrate,true);
   ArraySetAsSeries(maValL,true);
   ArraySetAsSeries(maValM,true);
   ArraySetAsSeries(maValR,true);
// Actualiza la informacion del simbolo
   if(!miSimbolo.RefreshRates())
     {
      Alert("Error cogiendo la ultima quota de precios - error:",GetLastError());
      return;
     }
   if(CopyRates(_Symbol,_Period,0,4,mrate)<0)
     {
      Alert("Error copiando rates/history data - error:",GetLastError());

      return;
     }
//------------------------------------------------------------------------
//------todo este bloque sirve para poder identificar nuevas barras-------
//------------------------------------------------------------------------
   static datetime Prev_time;
   datetime  Bar_time[1];
   Bar_time[0] = mrate[0].time;
   if(Prev_time==Bar_time[0])
     {
      return;
     }
   Prev_time=Bar_time[0];
//-------ESTO SIRVE PARA PODER  COPIAR EN UN ARRAY ELEMENTOS DE LA MEDIA MOVIL----------------

//+------------------------------------------------------------------+
//| CopyBuffer                                                       |
//| int  CopyBuffer( 
//| int       indicator_handle,     // manejador del indicador                                                                   |
//| int       buffer_num,           // número del buffer del indicador
//| int       numero de valores                           
//| double arreglo al que se copiara la informacion
//+------------------------------------------------------------------+

   if(CopyBuffer(maHandleR,0,0,3,maValR)<3)
     {
      Alert("Error copiando el indicador buffer de la Media - error",GetLastError());
      return;
     }
   if(CopyBuffer(maHandleM,0,0,3,maValM)<3)
     {
      Alert("Error copiando el indicador buffer de la Media - error",GetLastError());
      return;
     }
   if(CopyBuffer(maHandleL,0,0,3,maValL)<3)
     {
      Alert("Error copiando el indicador buffer de la Media - error",GetLastError());
      return;
     }
// Copia el valor del precio de cierre de la penultima vela en la variable p_cierre
   p_cierre=mrate[1].close;

/**
   * Comienza el Algoritmo 
   * la especificacion del mismo se encuentra en el archivo Diagrama Beatriz
   */
   if(lateralidad(mrate)){
      lateralidad = true;
      cerrarPosicion();
      posisionAbierta = false;
      
   }
   
   
   if(lateralidad && terminoLateralidad(mrate)){
     //Comment("MErcado EN TENDENCIA");
     lateralidad = false;
     if(maValR[0]< maValM[0]){
      cruceBajista = true;
       cruceAlcista = false;
     }else {
      cruceAlcista = true;
      cruceBajista = false;
     }
     
   }
   if(!lateralidad){
      //Comment("CruceAlcista =",cruceAlcista, " cruceBajista = ",cruceBajista);
   if(cruceAlcista && !posisionAbierta )
     {
      if(cruceLineas('L','M'))
        {
         comprar();
         posisionAbierta = true;
         cruceAlcista = false;
         compraActiva = true;
           }else{
         return;
        }
     }
   if(cruceBajista && !posisionAbierta)
     {
      if(cruceLineas('M','L'))
        {
         vender();
         posisionAbierta = true;
         cruceBajista= false;
         ventaActiva = true;
           }else{
         return;
        }
     }
// Caso : Bajista
   if(cruceLineas('R','M'))
     {
      if(compraActiva)
        {
         cerrarPosicion();
         posisionAbierta = false;
         compraActiva = false ;
         cruceBajista= true;
         // En teoria debo preguntar si la media lenta esta por debajo de la media con el promedio de Media
         return;
     }else{
         cruceBajista=true;
         cruceAlcista = false;
         if(maValM[0]< maValL[0])
           {
            if(vender())
              {
              posisionAbierta =true;
               cruceBajista= false;
               ventaActiva = true;
   
              }
            return;
           }
        }
     }
// Caso Alcista
   if(cruceLineas('M','R'))
     {
      if(ventaActiva)
        {
         cerrarPosicion();
         posisionAbierta = false;
         ventaActiva = false;
         
         cruceAlcista= true;
         return;
           }else{
         cruceAlcista=true;
         cruceBajista = false;
         //if(cruceLineas('M','L')) // Esta mal porque lo que dice es que esperamos el cruce inverso pero eso no es del todo cierto 
          if(maValM[0]> maValL[0])  
           {
            if(comprar())
              {
              posisionAbierta = true;
               cruceAlcista = false;
               compraActiva = true;
              }
            return;
           }
        }
     }
   
   }
   

  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
bool cruceLineas(char atravieza,char atravezado)
  {
   bool res=false;
   double atraviezaAr[];
   double atravezadoAr[];
   if(atravieza=='R')
     {
      ArrayCopy(atraviezaAr,maValR,0,0,WHOLE_ARRAY);
      //atraviezaAr = &maValR; 
     }
   if(atravieza=='M')
     {
      //atraviezaAr = &maValM; 
      ArrayCopy(atraviezaAr,maValM,0,0,WHOLE_ARRAY);
     }
   if(atravieza=='L')
     {
      //atraviezaAr = &maValL; 
      ArrayCopy(atraviezaAr,maValL,0,0,WHOLE_ARRAY);
     }
   if(atravezado=='R')
     {
      //atravezadoAr = &maValR; 
      ArrayCopy(atravezadoAr,maValR,0,0,WHOLE_ARRAY);
     }
   if(atravezado=='M')
     {
      //atravezadoAr = &maValM; 
      ArrayCopy(atravezadoAr,maValM,0,0,WHOLE_ARRAY);
     }
   if(atravezado=='L')
     {
      //atravezadoAr = &maValL; 
      ArrayCopy(atravezadoAr,maValL,0,0,WHOLE_ARRAY);
     }
   if((atraviezaAr[0]>atravezadoAr[0]) && (atraviezaAr[1]<atravezadoAr[1]))
     {
      res=true;
     }
   return res;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool comprar()
  {
   bool res=false;
   double miprice=NormalizeDouble(miSimbolo.Ask(),_Digits);
   double SL = NormalizeDouble(miSimbolo.Ask()-StopLoss * _Point,_Digits);
   double TP = NormalizeDouble(miSimbolo.Ask()+TakeProfit * _Point,_Digits);
   //if(miTrade.Buy(Lot,_Symbol,miprice,SL,TP,""))
   if(!posisionAbierta && miTrade.Buy(Lot,_Symbol,miprice,0,0,""))
     {
      ticket=miTrade.ResultDeal();
      res=true;
        }else{
      Alert("La orden de compra con el Volumne ",miTrade.RequestVolume(),
            " ,SL= ",miTrade.RequestSL()," ,TP= ",miTrade.RequestTP(),
            "no se ha podido ejecutar por el error ",miTrade.ResultRetcodeDescription());

     }

   return res;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool vender()
  {
   bool res=false;
   double miprice=NormalizeDouble(miSimbolo.Bid(),_Digits);
   double SL = NormalizeDouble(miSimbolo.Bid()+StopLoss * _Point,_Digits);
   double TP = NormalizeDouble(miSimbolo.Bid()-TakeProfit * _Point,_Digits);
   //if(miTrade.Sell(Lot,_Symbol,miprice,SL,TP,""))
  if(!posisionAbierta && miTrade.Sell(Lot,_Symbol,miprice,SL,0,""))
     {
      //Alert("La orden de compra se ha realizado correctamente con el Tiket",miTrade.ResultDeal(),"!!"); // Esta Funcion te devuelve el ticket que va a ser indispensable
      ticket=miTrade.ResultDeal();
      res=true;
        }else{
      Alert("La orden de compra con el Volumne ",miTrade.RequestVolume(),
            " ,SL= ",miTrade.RequestSL()," ,TP= ",miTrade.RequestTP(),
            "no se ha podido ejecutar por el error ",miTrade.ResultRetcodeDescription());
      res=false;
     }
   return res;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void cerrarPosicion()
  {
  //operationClose++;
   miTrade.PositionClose(ticket,dev);
   
  }
//+------------------------------------------------------------------+
// Funcion que determina lateralidad pero este no es el mejor 
bool lateralidad(MqlRates &mrates[]){
   bool res = false;
   double UValor = NormalizeDouble(maValM[0],_Digits); 
   double PUValor = NormalizeDouble(maValM[1],_Digits);      
   double APUValor = NormalizeDouble(maValM[2],_Digits); 
   if((UValor > mrates[1].low && UValor < mrates[1].high) && (PUValor > mrates[2].low && PUValor < mrates[2].high) && (APUValor > mrates[3].low && APUValor < mrates[3].high)){     
      res = true;
   }
   return res ;
}

bool terminoLateralidad(MqlRates &mrates[]){
   bool res = false;
//   double UValor = NormalizeDouble(mrates[0].close,_Digits); 
//   double PUValor = NormalizeDouble(mrates[1].close,_Digits);      
//   double APUValor = NormalizeDouble(mrates[2].close,_Digits);
//   double AAPUValor = NormalizeDouble(mrates[3].close,_Digits); 
//   
   double UValor = NormalizeDouble(maValR[0],_Digits); 
   double PUValor = NormalizeDouble(maValR[1],_Digits);      
   double APUValor = NormalizeDouble(maValR[2],_Digits);
   
//   Comment(NormalizeDouble(MathAbs(AAPUValor - APUValor),_Digits), 
//   "\n", NormalizeDouble(MathAbs(APUValor - PUValor),_Digits), 
//   "\n", NormalizeDouble(MathAbs(PUValor - UValor),_Digits)); 
//   
   //double temp2 = (NormalizeDouble(MathAbs(APUValor - UValor),_Digits)*pow(10,_Digits)) ;
   
//   double angulo; 
//   if(NormalizeDouble(MathAbs(PUValor - UValor) ,_Digits)== 0){
//      angulo = 0 ;
//   
//   } else{
//   
//      angulo = MathArccos(1/(NormalizeDouble(MathAbs(PUValor - UValor),_Digits)*pow(10,_Digits)));
//   }
   double temp = (NormalizeDouble(MathAbs(PUValor - UValor),_Digits)*pow(10,_Digits)); 
   //double pendiente = NormalizeDouble(MathAbs(APUValor - APUValor),_Digits) - NormalizeDouble(MathAbs(PUValor - UValor),_Digits);
   Comment("El valor la media Rapida U " , UValor, " Rapida PU ", PUValor," el resultado es = " ,temp); 
   if(temp > 30  ){
      res = true;
   }
   return res ;
}
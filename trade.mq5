//+------------------------------------------------------------------+
//|                                                        trade.mq5 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\Trade.mqh>
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
 
input int numeroMagico = 1234;
input double lote = 0.1;
input int ganancia = 1000 ; 
input int perdida = 100 ; 
CTrade trade;
int OnInit()
  {
//---
   trade.SetExpertMagicNumber(numeroMagico);
   
         
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   int digitos = (int)SymbolInfoInteger(_Symbol,SYMBOL_DIGITS);
   double puntos = SymbolInfoDouble(_Symbol,SYMBOL_POINT);
   double precioVenta = SymbolInfoDouble(_Symbol,SYMBOL_BID);
   double SL = precioVenta - ganancia * puntos; 
   SL = NormalizeDouble(SL,digitos);
   double precioApertura = SymbolInfoDouble(_Symbol,SYMBOL_ASK);
   double TP = precioVenta + ganancia * puntos; 
   TP = NormalizeDouble(TP,digitos);
   comprar(precioApertura,SL,TP);
      
  }
//+------------------------------------------------------------------+
void comprar(double price,double SL,double TP){
   if(!trade.Buy(lote,_Symbol,price,SL,TP,"")){
         Print("La orden no se ha podido introducir el error es : ",trade.ResultRetcode(), " - ", trade.ResultRetcodeDescription());
      }else{
         Print("La orden no se ha podido introducir");
      }
}
void venta(){

if(!trade.Sell(lote)){
         Print("La orden no se ha podido introducir el error es : ",trade.ResultRetcode(), " - ", trade.ResultRetcodeDescription());
      }else{
         Print("La orden no se ha podido introducir");
      }
}      
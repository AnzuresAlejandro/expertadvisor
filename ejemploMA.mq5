//+------------------------------------------------------------------+
//|                                                    ejemploMA.mq5 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\Trade.mqh>
#include <Trade\PositionInfo.mqh>
#include <Trade\SymbolInfo.mqh>
#include <Trade\AccountInfo.mqh>

//+------------------------------------------------------------------+
//| Expert global variables                                          |
//+------------------------------------------------------------------+

CPositionInfo misPosiciones;
CTrade miTrade; 
CAccountInfo miCuenta;
CSymbolInfo miSimbolo;
int minBar = 20;
extern int numeroMagico = 12345 ;
extern double lot = 0.1; 
extern ulong desviacion=100;
extern double SL = 100;
extern double TP = 240;
extern int MA_Period =15 ; 

int maHandle; 
double maVal[];

bool revisar(){
   bool siTrading = false ;
   if(miCuenta.TradeAllowed() && miCuenta.TradeExpert() && miSimbolo.IsSynchronized()){
      int mbars = Bars(_Symbol,_Period);
      if(mbars> minBar){
         siTrading = true;
      }  
   }
   return siTrading;   
}

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
      if(revisar()){
      
      }else{
         Alert("Por algun motivo el tranding no esta permitido");
         return ;
      } 
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                            InformacionCuenta.mq5 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\AccountInfo.mqh>
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   CAccountInfo cuenta;
   long login = cuenta.Login();
   Print("Cuenta:=",login);
   ENUM_ACCOUNT_TRADE_MODE cuenta_tipo = cuenta.TradeMode();
   
   Print("Cuenta Tipo = ",cuenta_tipo);
   
   Print("Balance:= ",cuenta.Balance());
   Print("Beneficio:= ",cuenta.Profit());
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   
  }
//+------------------------------------------------------------------+
